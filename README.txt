Order of teaching these programs:

1. led.py
Sequential ordering of code
Modules and functions exported by them

2. led_blink.py
Loops with a counter


3. switch_simple.py
User defined functions
Infinite loops and signal handlers

4. switch_states.py
Notion of state changes in GPIO pins
Use of friendly constants
Lots of logical thinking

5. switch_led_onoff.py
No new concepts
Mix learnings of led.py and switch_states.py

6. photocell.py
No new concepts
Apply learnings of switch_states.py

7. photocell_led_onoff.py
No new concepts
Apply learnings of switch_led_onoff.py

8. switch_music_player.py
Execute CLI commands from within python
Explain why it is necessary to kill process

9. photocell_music_player.py
No new concepts
Apply learnings of switch_music_player.py




