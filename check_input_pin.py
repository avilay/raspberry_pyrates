import sys
import RPi.GPIO as gpio
import signal
import time

IN_PIN = 23

def setup():
    gpio.setmode(gpio.BCM)
    gpio.setup(IN_PIN, gpio.IN)

def cleanup(signal, frame):
    gpio.cleanup()
    sys.exit(0)

def main():
    signal.signal(signal.SIGINT, cleanup)
    setup()
    while True:
        print('Value of pin {} is {}'.format(str(IN_PIN), str(gpio.input(IN_PIN))))
        time.sleep(1)
        

if __name__ == '__main__':
    main()
    
