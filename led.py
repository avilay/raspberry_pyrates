import time
import RPi.GPIO as gpio

gpio.setmode(gpio.BCM)
gpio.setup(22, gpio.OUT)
gpio.output(22, gpio.HIGH)
time.sleep(5)
gpio.cleanup()
