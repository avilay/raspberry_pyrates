import time
import RPi.GPIO as gpio

gpio.setmode(gpio.BCM)
gpio.setup(18, gpio.OUT)

ctr = 0
while ctr < 5:
    gpio.output(18, gpio.HIGH)
    time.sleep(0.5)
    gpio.output(18, gpio.LOW)
    time.sleep(0.5)
    ctr = ctr + 1
    
gpio.cleanup()
