import signal
import sys
import RPi.GPIO as gpio

LIGHT = 1
DARK = 2

def setup():
    gpio.setmode(gpio.BCM)
    gpio.setup(23, gpio.IN)
    gpio.setup(18, gpio.OUT)

def cleanup(signal, frame):
    gpio.cleanup()
    sys.exit(0)

def is_light():
    return gpio.input(23) == gpio.LOW

def is_dark():
    return gpio.input(23) == gpio.HIGH

def turn_led_on():
    gpio.output(18, gpio.HIGH)

def turn_led_off():
    gpio.output(18, gpio.LOW)

def handle_state_change(curr_light_state):
    if curr_light_state == LIGHT:
        print("It is light. Turning LED off.")
        turn_led_off()
    elif curr_light_state == DARK:
        print("It is dark. Turning LED on.")
        turn_led_on()
    else:
        print("Unknown light state!!")

signal.signal(signal.SIGINT, cleanup)
setup()
curr = LIGHT
prev = LIGHT
handle_state_change(curr)
while True:
    if is_light():
        curr = LIGHT
    elif is_dark():
        curr = DARK

    if curr != prev:
        handle_state_change(curr)

    prev = curr

