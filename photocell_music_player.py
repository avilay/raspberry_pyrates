import os
import signal
import sys
import time
import RPi.GPIO as gpio

LIGHT = 1
DARK = 2

def setup():
    gpio.setmode(gpio.BCM)
    gpio.setup(23, gpio.IN)
    gpio.setup(18, gpio.OUT)
    os.system('amixer cset numid=3 1')

def cleanup(signal, frame):
    os.system('kill `pgrep mpg`')
    gpio.cleanup()
    sys.exit(0)

def is_light():
    return gpio.input(23) == gpio.LOW

def is_dark():
    return gpio.input(23) == gpio.HIGH

def turn_led_on():
    gpio.output(18, gpio.HIGH)

def turn_led_off():
    gpio.output(18, gpio.LOW)

def handle_state_change(curr_state):    
    os.system('kill `pgrep mpg`')    
    if curr_state == DARK:        
        turn_led_on()
        os.system('espeak "Changing track."')        
        time.sleep(2)
        os.system('mpg123 /home/pi/viva.mp3 &')
    elif curr_state == LIGHT:
        turn_led_off()
        os.system('espeak "Changing track."')
        time.sleep(2)
        os.system('mpg123 /home/pi/waiting.mp3 &')
    else:
        print("Unknown switch state!!")

signal.signal(signal.SIGINT, cleanup)
setup()

curr = LIGHT
prev = LIGHT
handle_state_change(curr)
while True:
    if is_light():
        curr = LIGHT
    elif is_dark():
        curr = DARK

    if curr != prev:
        handle_state_change(curr)

    prev = curr

