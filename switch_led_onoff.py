import signal
import sys
import RPi.GPIO as gpio

SWITCH_ON = 1
SWITCH_OFF = 2
SWITCH_IN_PIN = 18
LED_OUT_PIN = 22

def setup():
    gpio.setmode(gpio.BCM)
    gpio.setup(SWITCH_IN_PIN, gpio.IN)
    gpio.setup(LED_OUT_PIN, gpio.OUT)

def cleanup(signal, frame):
    gpio.cleanup()
    sys.exit(0)

def is_switch_on():
    return gpio.input(SWITCH_IN_PIN) == gpio.LOW

def is_switch_off():
    return gpio.input(SWITCH_IN_PIN) == gpio.HIGH

def turn_led_on():
    gpio.output(LED_OUT_PIN, gpio.HIGH)

def turn_led_off():
    gpio.output(LED_OUT_PIN, gpio.LOW)

def handle_state_change(curr_switch_state):
    if curr_switch_state == SWITCH_ON:
        print("Switch has been turned on. Turning LED on.")
        turn_led_on()
    elif curr_switch_state == SWITCH_OFF:
        print("Switch has been turned off. Turning LED off.")
        turn_led_off()
    else:
        print("Unknown switch state!!")

signal.signal(signal.SIGINT, cleanup)
setup()

curr = SWITCH_OFF
prev = SWITCH_OFF
while True:
    if is_switch_on():
        curr = SWITCH_ON
    elif is_switch_off():
        curr = SWITCH_OFF

    if curr != prev:
        handle_state_change(curr)

    prev = curr

