import os
import signal
import sys
import time
import RPi.GPIO as gpio

SWITCH_ON = 1
SWITCH_OFF = 2

def setup():
    gpio.setmode(gpio.BCM)
    gpio.setup(23, gpio.IN)
    gpio.setup(18, gpio.OUT)
    os.system('amixer cset numid=3 1')

def cleanup(signal, frame):
    os.system('kill `pgrep mpg`')
    gpio.cleanup()
    sys.exit(0)

def is_switch_on():
    return gpio.input(23) == gpio.LOW

def is_switch_off():
    return gpio.input(23) == gpio.HIGH

def turn_led_on():
    gpio.output(18, gpio.HIGH)

def turn_led_off():
    gpio.output(18, gpio.LOW)

def handle_state_change(curr_switch_state):    
    os.system('kill `pgrep mpg`')    
    if curr_switch_state == SWITCH_ON:        
        turn_led_on()
        os.system('espeak "Changing track."')        
        time.sleep(3)
        os.system('mpg123 /home/pi/viva.mp3 &')
    elif curr_switch_state == SWITCH_OFF:
        turn_led_off()
        os.system('espeak "Changing track."')
        time.sleep(3)
        os.system('mpg123 /home/pi/waiting.mp3 &')
    else:
        print("Unknown switch state!!")

signal.signal(signal.SIGINT, cleanup)
setup()

curr = SWITCH_OFF
prev = SWITCH_OFF
handle_state_change(curr)
while True:
    if is_switch_on():
        curr = SWITCH_ON
    elif is_switch_off():
        curr = SWITCH_OFF

    if curr != prev:
        handle_state_change(curr)

    prev = curr

