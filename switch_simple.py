import signal
import sys
import RPi.GPIO as gpio

def cleanup(signal, frame):
    gpio.cleanup()
    sys.exit(0)

def is_switch_on():
    return gpio.input(18) == gpio.LOW

def is_switch_off():
    return gpio.input(18) == gpio.HIGH

signal.signal(signal.SIGINT, cleanup)

gpio.setmode(gpio.BCM)
gpio.setup(18, gpio.IN)

while True:
    if is_switch_on():
        print('***********SWITCH ON')
    elif is_switch_off():
        print('-----------SWITCH OFF')



