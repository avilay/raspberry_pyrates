import signal
import sys
import RPi.GPIO as gpio

SWITCH_ON = 1
SWITCH_OFF = 2

def setup():
    gpio.setmode(gpio.BCM)
    gpio.setup(23, gpio.IN)

def cleanup(signal, frame):
    gpio.cleanup()
    sys.exit(0)

def is_switch_on():
    return gpio.input(23) == gpio.LOW

def is_switch_off():
    return gpio.input(23) == gpio.HIGH

def handle_state_change(curr_switch_state):
    if curr_switch_state == SWITCH_ON:
        print("Switch has been turned on")
    elif curr_switch_state == SWITCH_OFF:
        print("Switch has been turned off")
    else:
        print("Unknown switch state!!")

signal.signal(signal.SIGINT, cleanup)
setup()
curr = SWITCH_OFF
prev = SWITCH_OFF
while True:
    if is_switch_on():
        curr = SWITCH_ON
    elif is_switch_off():
        curr = SWITCH_OFF

    if curr != prev:
        handle_state_change(curr)

    prev = curr

